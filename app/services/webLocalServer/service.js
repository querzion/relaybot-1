const BasicService = use('classes/BasicService');
const path         = require('path');
const express      = require('express');
const socketio     = require('socket.io');
const bodyParser   = require('body-parser');

/**
 * @alias Service.webLocalServer
 * @extends _.BasicService
 */
class Service extends BasicService {

  /**
   * runs the service code when called
   */
  init() {
    this.server = express();

    // configure twig
    this.server.set('view engine', 'twig');
    this.server.set('twig options', {
        strict_variables: false
    });

    // configure static assets folder
    this.server.use(express.static(path.join(__dirname, '/web')));

    // configure body parsing
    this.server.use(bodyParser.json());
    this.server.use(bodyParser.urlencoded({extended: true}));

    // start server and socket.io
    this._auriga.on('ready', () => {
      const server = this.server.listen(this._settings.port, () => {
        this.socketio = socketio.listen(server);
  
        this._settings.public_domain = this._settings.public_domain || 'localhost';
        this.info(`Server now online at ${this._settings.public_domain}:${this._settings.port}`);
        this.register();
      });
    });
  }

  /**
   * registers a directory as a static file source
   * @param {string} directory the directory to register
   * @param {object} module the module to use
   * @return {string} the url associated with the registered directory
   */
  registerStatic(directory, module) {
    directory  = path.normalize(directory);
    let folder = directory.split(path.sep).pop();

    this.server.use(
      `/${module._type}/${module._name}/${folder}`,
      express.static(directory)
    );

    const url = `http://${this._settings.public_domain}:${this._settings.port}/${module._type}/${module._name}/${folder}`;
    return url;
  }

  /**
   * creates a browseable web route via GET
   * @param {string} route the url route to register
   * @param {function} callback the callback function to handle the route
   */
  addGetRoute(route, callback) {
    this.server.get(route, (req, res) => {
      callback(req, res);
    });
  }

  /**
   * creates a browseable web route via POST
   * @param {string} route the url route to register
   * @param {function} callback the callback function to handle the route
   */
  addPostRoute(route, callback) {
    this.server.post(route, (req, res) => {
      callback(req, res);
    });
  }
};

module.exports = Service;
