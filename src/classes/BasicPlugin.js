/**
 * class representing a basic plugin
 * @alias _.BasicPlugin
 */
class BasicPlugin {
  /** creates the plugin
   * @param {object} auriga the auriga client reference
   * @param {string} name the plugin name decided by auriga
   * @param {object} settings the plugin settings loaded from json
  */
  constructor(auriga, name, settings) {
    this._auriga     = auriga;
    this._name       = name;
    this._settings   = settings;
    this._registered = false;
    this._type       = 'plugin';
  }

  /**
   * code to run on load
   */
  init() {
    this.register();
  }

  /**
   * registers the plugin
   */
  register() {
    if (!this._registered) {
      this._registered = true;
      this._auriga.registerPlugin(this._name, this);
    }
  }

  /**
   * logs informational messages to the console
   * @param {string} msg the message to log
   */
  info(msg) {
    this._auriga.info(`[Plugin:${this._name}]: ${msg}`);
  }

  /**
   * logs a message to the console
   * @param {string} msg the message to log
   */
  log(msg) {
    this._auriga.log(`[Plugin:${this._name}]: ${msg}`);
  }

  /**
   * logs a warning message to the console
   * @param {string} msg  the warning message to log
   */
  warn(msg) {
    this._auriga.warn(`[Plugin:${this._name}]: ${msg}`);
  }

  /**
   * logs an error message to the console and closes the script
   * @param {string} msg the error message to log
   */
  error(msg) {
    this._auriga.error(`[Plugin:${this._name}]: ${msg}`);
  }
};

module.exports = BasicPlugin;
